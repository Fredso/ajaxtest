SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `bcsnetDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bcsnetDB`;

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL,
  `PASSWRD` varchar(20) NOT NULL,
  `CATEGORY` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`ID`, `NAME`, `PASSWRD`, `CATEGORY`) VALUES
(1, 'admin', 'admin123', '');

DROP TABLE IF EXISTS `tbl_content`;
CREATE TABLE `tbl_content` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDNAME` varchar(255) NOT NULL,
  `TITLE` varchar(255),
  `TEXT` varchar(255),
  `IMAGE` varchar(255),
  `LINK` varchar(255),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_content` (`ID`, `IDNAME`, `TITLE`, `TEXT`, `IMAGE`, `LINK`) VALUES
(2, 'headTitle', 'BCSA', null, null, null);

/*INSERT INTO `tbl_content` (`ID`, `IDNAME`, `TITLE`, `TEXT`, `IMAGE`, `LINK`) VALUES
(2, 'headTitle', 'BCS', null, null, null),
(3, 'navbarToggleButton', null, null, null, null),
(4, 'homeA', null, null, null, null),
(5, 'navbarMenuOption1', 'Home', null, null, null);
*/
SELECT * FROM `tbl_content`