<?php 

include_once('xyz.php');

// $error_level = error_reporting(0);
// $dbh = mysql_connect();
// error_reporting($error_level);

////////////////////////////////////////////////////////////////
/////////              MySQLi Functions           //////////////
////////////////////////////////////////////////////////////////

function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

////////////////////////////////////////////////////////////////
/////////              Show all Data              //////////////
////////////////////////////////////////////////////////////////

function get_all_json() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_content";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "idName" => $row['IDNAME'],
            "title" => $row['TITLE'],
            "text" => $row['TEXT']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}
